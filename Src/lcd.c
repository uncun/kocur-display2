
/** Put this in the src folder **/

#include "lcd.h"
extern I2C_HandleTypeDef hi2c1; // change your handler here accordingly

#define SLAVE_ADDRESS_LCD 0x4E// change this according to ur setup
#define LCD_CLEAR_DISPLAY 0x01
#define LCD_RETURN_HOME 0x02
#define LCD_ENTRYMODE 0x04
#define LCD_DISPLAY_CONTROL 0x08
#define LCD_CURSORDISPLAY_SHIFT 0x10
#define LCD_FUNCTION_SET 0x20
char lcd_buf[LCD_ROWS][LCD_COLS];
char lcd_buf_old[LCD_ROWS][LCD_COLS];
uint8_t lcd_buf_x, lcd_buf_y;
void lcd_device_check(void)
{
	HAL_Delay(50);
	while (HAL_I2C_IsDeviceReady(&hi2c1, SLAVE_ADDRESS_LCD,3,1000)!=HAL_OK)
	{

	}
}

void lcd_send_cmd (char cmd)
{
  char data_u, data_l;
	uint8_t data_t[4];
	data_u = (cmd&0xF0);
	data_l = ((cmd<<4)&0xF0);
	data_t[0] = data_u|0x0C;  //en=1, rs=0
	data_t[1] = data_u|0x08;  //en=0, rs=0
	data_t[2] = data_l|0x0C;  //en=1, rs=0
	data_t[3] = data_l|0x08;  //en=0, rs=0
	HAL_I2C_Master_Transmit (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 1000);
}

void lcd_send_data (char data)
{
	char data_u, data_l;
	uint8_t data_t[4];
	data_u = (data&0xf0);
	data_l = ((data<<4)&0xf0);
	data_t[0] = data_u|0x0D;  //en=1, rs=1
	data_t[1] = data_u|0x09;  //en=0, rs=1
	data_t[2] = data_l|0x0D;  //en=1, rs=1
	data_t[3] = data_l|0x09;  //en=0, rs=1
	HAL_I2C_Master_Transmit (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 1000);
}

void lcd_clear (void)
{
	lcd_send_cmd (0x01);
	HAL_Delay(100);
	for (int i=0; i<100; i++)
	{
		lcd_send_data (' ');
	}
}

void lcd_init (void)
{
	// 4 bit initialisation
	HAL_Delay(50);  // wait for >40ms
	lcd_send_cmd (0x03);
	HAL_Delay(5);  // wait for >4.1ms
	lcd_send_cmd (0x03);
	HAL_Delay(2);  // wait for >100us
	lcd_send_cmd (0x03);
	HAL_Delay(100);
	lcd_send_cmd (0x02);  // 4bit mode
	HAL_Delay(100);

// lcd_send_cmd(LCD_CLEAR_DISPLAY);
// HAL_Delay(1000);
// lcd_send_cmd(LCD_RETURN_HOME);
// HAL_Delay(5);
// lcd_send_cmd(LCD_FUNCTION_SET|0x00|0x08|0x00);
// HAL_Delay(5);
// lcd_send_cmd(LCD_DISPLAY_CONTROL|0x04|0x00|0x00);
// HAL_Delay(5);
// lcd_send_cmd(0x80);
// HAL_Delay(500);
  // dislay initialisation
	lcd_send_cmd (0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
	HAL_Delay(1);
	lcd_send_cmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
	HAL_Delay(1);
	lcd_send_cmd (0x01);  // clear display
	HAL_Delay(1);
	HAL_Delay(1);
	lcd_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
	HAL_Delay(1);
	lcd_send_cmd (0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
	HAL_Delay(10);
}

void lcd_send_string (char *str)
{
	while (*str) lcd_send_data (*str++);
}

void lcd_locate (uint8_t x, uint8_t y)
{
		switch(y)
		{
				case 0:
					lcd_send_cmd( LCDC_SET_DDRAM | (LCD_LINE1 + x) );
					break;
				case 1:
					lcd_send_cmd( LCDC_SET_DDRAM | (LCD_LINE2 + x) );
					break;
				case 2:
					lcd_send_cmd( LCDC_SET_DDRAM | (LCD_LINE3 + x) );
					break;
				case 3:
					lcd_send_cmd( LCDC_SET_DDRAM | (LCD_LINE4 + x) );
					break;
		}

}


void lcd_str_XY(uint8_t x, uint8_t y, char * str)
{
		lcd_locate(x,y);

		while(*str)
			lcd_send_data(*str++);


}




char 	lcd_buf[LCD_ROWS][LCD_COLS];
char 	lcd_buf_old[LCD_ROWS][LCD_COLS];
uint8_t lcd_buf_x, lcd_buf_y;
void buf_locate(uint8_t x, uint8_t y)
{
	lcd_buf_x = x;
	lcd_buf_y = y;
}
void buf_char(char c)
{
	if (lcd_buf_x < LCD_COLS && lcd_buf_y < LCD_ROWS)
	{
		lcd_buf[lcd_buf_y][lcd_buf_x] = c;
		lcd_buf_x++;
		if (lcd_buf_x == LCD_COLS) {
			lcd_buf_x = 0;
			lcd_buf_y++;
			if (lcd_buf_y == LCD_ROWS)
				lcd_buf_y = 0;
		}
	}
}
void buf_clear(void)
{
	for(uint8_t y=0; y<LCD_ROWS; y++)
	{
		for(uint8_t x=0; x<LCD_COLS; x++)
		{
			lcd_buf[y][x]=' ';
		}
	}
	lcd_buf_x=0; lcd_buf_y=0;
}
void buf_clear_menu(void)
{
	for(uint8_t y=1; y<LCD_ROWS; y++)
	{
		for(uint8_t x=0; x<LCD_COLS; x++)
		{
			lcd_buf[y][x]=' ';
		}
	}
	lcd_buf_x=0; lcd_buf_y=0;
}
void lcd_refresh(void)
{
	static uint8_t locate_flag = 0;
	for(uint8_t y=0; y<LCD_ROWS; y++)
	{
		lcd_locate( 0, y );
		for(uint8_t x=0; x<LCD_COLS; x++)
		{
			if( lcd_buf[y][x] != lcd_buf_old[y][x] )
			{
				if( !locate_flag )
					lcd_locate( x, y );
				lcd_send_data( lcd_buf[y][x] );
				lcd_buf_old[y][x] = lcd_buf[y][x];
				locate_flag = 1;
			}
			else
				locate_flag = 0;
		}
	}
}
/* Additional functions */
void buf_str(char *text)
{
	while(*text)
		buf_char(*text++);
}
void buf_str_XY(uint8_t x, uint8_t y, char *text)
{
	buf_locate(x,y);
	while(*text)
		buf_char(*text++);
}
void buf_int(uint8_t value)
{
	char string[5];
	itoa(value, string, 10);
	buf_str(string);
}


