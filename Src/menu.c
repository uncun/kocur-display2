#include <string.h>
#include "menu.h"
#include "lcd.h"


//menu components def (*name, *next, *prev, *child, *parent, (*menu_fun) ) 
menu_t menu1 = {"PWM TIM2 CH1 ", &menu2, &menu6, &sub_menu1_1, NULL, NULL };
    menu_t sub_menu1_1 = { "Duty", &sub_menu1_2, &sub_menu1_2, NULL, &menu1,duty_callback};
    menu_t sub_menu1_2 = { "F", NULL, &sub_menu1_1, NULL, &menu1, NULL};
menu_t menu2 = {"ELEMENT 2", &menu3, &menu1, &sub_menu2_1, NULL, NULL };
    menu_t sub_menu2_1 = { "e2_1", &sub_menu2_2, NULL, NULL, &menu2, set_led};
    menu_t sub_menu2_2 = { "e2_2", NULL, &sub_menu2_1, NULL, &menu2, reset_led};
menu_t menu3 = {"ELEMENT 3", &menu4, &menu2, NULL, NULL, NULL };
menu_t menu4 = {"ELEMENT 4", &menu5, &menu3, NULL, NULL, NULL };
menu_t menu5 = {"ELEMENT 5", &menu6, &menu4, NULL, NULL, NULL };
menu_t menu6 = {"ELEMENT 6", NULL, &menu5, NULL, NULL, NULL };

menu_t *currentPointer = &menu1;

/////////////////////////////////////////////////////////
void (*key_next)(void)=&menu_next;
void (*key_prev)(void)=&menu_prev;
void (*key_enter)(void)=&menu_enter;
void (*key_back)(void)=&menu_back;

 void key_enter_press(void)
 {
     
     if(key_enter)
     (*key_enter)();
     
 }
 void key_back_press(void)
 {
     if(key_back)
     (*key_back)();

 }
 void key_next_press(void)
 {
     if(key_next)
     (*key_next)();

 }
 void key_prev_press(void)
 {
     if(key_prev)
     (*key_prev)();

 }
/////////////////////////////////////////////////////////

uint8_t menu_index;
uint8_t lcd_row_pos;
uint8_t lcd_row_pos_level1, lcd_row_pos_level2;

extern char lcd_buf[LCD_ROWS][LCD_COLS];

void menu_refresh(void)
{
    menu_t *temp;
    uint8_t i;
    uint8_t center;
    
    if (currentPointer->parent) 
        temp=(currentPointer->parent)->child;
    else
        temp = &menu1;

    for(i=0; i!=menu_index - lcd_row_pos; i++)
    {
        temp= temp->next;
    }

    lcd_clear();

    for(i=0; i<LCD_ROWS; i++)
    {
        lcd_locate(0,i);
        if(temp==currentPointer)
            lcd_send_data(62);
        else
            lcd_send_data(' ');

        lcd_locate(2,i);
        lcd_send_string(temp->name);

        temp = temp->next;
        if(!temp)
            break;
    }
}

uint8_t menu_get_index(menu_t *q)
{
    menu_t *temp;
    uint8_t i=0;

    if(q->parent) 
        temp=(q->parent)->child;
    else    
        temp=&menu1;

    while (temp != q)
    {
        temp=temp->next;
        i++;
    }
    return i;
}

uint8_t menu_get_level(menu_t *q)
{
    menu_t *temp=q;
    uint8_t i=0;

    if(!q->parent) 
        return 0;

    while (temp->parent != NULL )
    {
        temp=temp->parent;
        i++;
    }
    return i;
}

void menu_next(void)
{
    
    if (currentPointer->next)
    {
        currentPointer=currentPointer->next;
        menu_index++;
        if(++lcd_row_pos > LCD_ROWS-1)
            lcd_row_pos = LCD_ROWS-1;
    }
    else
    {
        menu_index=0;
        lcd_row_pos=0;

        if(currentPointer->parent)
            currentPointer=(currentPointer->parent)->child;
        else
            currentPointer=&menu1;
    }

    menu_refresh();

}

void menu_prev(void)
{
    currentPointer=currentPointer->prev;

    if (menu_index)
    {
      menu_index--;
      if (lcd_row_pos>0)
        lcd_row_pos--;
    }
    else
    {
       menu_index=menu_get_index(currentPointer);

        if(menu_index >= LCD_ROWS-1)
            lcd_row_pos= LCD_ROWS-1;
        else
            lcd_row_pos=menu_index;

    }

    menu_refresh();

}

void menu_enter(void)
{
    if(currentPointer->menu_fun)
        currentPointer->menu_fun();

    if(currentPointer->child)
    {
        switch(menu_get_level(currentPointer))
        {
            case 0:
                lcd_row_pos_level1=lcd_row_pos;
                break;
            case 1:
                lcd_row_pos_level2=lcd_row_pos;
                break;
        }
        menu_index=0;
        lcd_row_pos=0;
        currentPointer=currentPointer->child;

        menu_refresh();
    }    

}

void menu_back(void)
{
    

    if(currentPointer->parent)
    {
        switch(menu_get_level(currentPointer))
        {
            case 0:
                lcd_row_pos_level1=lcd_row_pos;
                break;
            case 1:
                lcd_row_pos_level2=lcd_row_pos;
                break;
        }
        currentPointer=currentPointer->parent;
        menu_index=menu_get_index(currentPointer);

        menu_refresh();
    }    

}

void reset_led(void)
{
GPIOA-> ODR &=~GPIO_PIN_5;
}

void set_led(void)
{
GPIOA-> ODR |= GPIO_PIN_5;
}

// extern void (*key_next)(void);
// extern void (*key_prev)(void);
// extern void (*key_enter)(void);
// extern void (*key_back)(void);
void duty_refresh(void);
void duty_up(void)
{
    if(TIM2->CCR1 < 55000) 
        TIM2->CCR1+=5500;
        // lcd_str_XY(1,1,"dupa");
        //   buf_str_XY(5,2,"miau");
    duty_refresh();
}
void duty_down(void)
{
    if(TIM2->CCR1 >= 5500) 
        TIM2->CCR1-=5500;
                // lcd_str_XY(1,1,"jaja");

    duty_refresh();
}
void duty_back(void)
{
    key_next=menu_next;
    key_back=menu_back;
    key_enter=menu_enter;
    key_prev=menu_prev;

    menu_refresh();
}
void duty_refresh(void)
{
    lcd_refresh();
   uint8_t percent=(TIM2->CCR1)/550;
     
    lcd_str_XY(0,0, "------  DUTY  ------");
    buf_clear_menu();

    memset(&lcd_buf[2][8],0xFF,percent/10);
    memset(&lcd_buf[2][8+percent/10],'-',10-percent/10);

    buf_locate(2,2);
    buf_int(percent);
    buf_locate(5,2);
    buf_char('%');
    buf_locate(2,3);
    buf_str("Duty =");
    buf_locate(10,3);
    buf_int((TIM2->CCR1)/1000);
    buf_locate(13,3);
    buf_str("*10^3");
lcd_refresh();


}
void duty_callback(void)
{
key_next=duty_up;
key_prev=duty_down;
key_enter=NULL;
key_back=duty_back;
lcd_refresh();
    duty_refresh();

}


