#include "ws2812.h"
 #include "stm32f4xx_hal.h"
#include "main.h"

uint8_t ws_buffer[LED_BUFFER_LENGTH];

extern SPI_HandleTypeDef hspi2;
const uint8_t leddata[256*4] = 
{ // size = 256 * 3
    0X44 , 0X44 , 0X44 , 0X44 , // 0
    0X44 , 0X44 , 0X44 , 0X47 , // 1
    0X44 , 0X44 , 0X44 , 0X74 , 
    0X44 , 0X44 , 0X44 , 0X77 , 
    0X44 , 0X44 , 0X47 , 0X44 , 
    0X44 , 0X44 , 0X47 , 0X47 , 
    0X44 , 0X44 , 0X47 , 0X74 , 
    0X44 , 0X44 , 0X47 , 0X77 , 
    0X44 , 0X44 , 0X74 , 0X44 , 
    0X44 , 0X44 , 0X74 , 0X47 , 
    0X44 , 0X44 , 0X74 , 0X74 , 
    0X44 , 0X44 , 0X74 , 0X77 , 
    0X44 , 0X44 , 0X77 , 0X44 , 
    0X44 , 0X44 , 0X77 , 0X47 , 
    0X44 , 0X44 , 0X77 , 0X74 , 
    0X44 , 0X44 , 0X77 , 0X77 , 
    0X44 , 0X47 , 0X44 , 0X44 , 
    0X44 , 0X47 , 0X44 , 0X47 , 
    0X44 , 0X47 , 0X44 , 0X74 , 
    0X44 , 0X47 , 0X44 , 0X77 , 
    0X44 , 0X47 , 0X47 , 0X44 , 
    0X44 , 0X47 , 0X47 , 0X47 , 
    0X44 , 0X47 , 0X47 , 0X74 , 
    0X44 , 0X47 , 0X47 , 0X77 , 
    0X44 , 0X47 , 0X74 , 0X44 , 
    0X44 , 0X47 , 0X74 , 0X47 , 
    0X44 , 0X47 , 0X74 , 0X74 , 
    0X44 , 0X47 , 0X74 , 0X77 , 
    0X44 , 0X47 , 0X77 , 0X44 , 
    0X44 , 0X47 , 0X77 , 0X47 , 
    0X44 , 0X47 , 0X77 , 0X74 , 
    0X44 , 0X47 , 0X77 , 0X77 , 
    0X44 , 0X74 , 0X44 , 0X44 , 
    0X44 , 0X74 , 0X44 , 0X47 , 
    0X44 , 0X74 , 0X44 , 0X74 , 
    0X44 , 0X74 , 0X44 , 0X77 , 
    0X44 , 0X74 , 0X47 , 0X44 , 
    0X44 , 0X74 , 0X47 , 0X47 , 
    0X44 , 0X74 , 0X47 , 0X74 , 
    0X44 , 0X74 , 0X47 , 0X77 , 
    0X44 , 0X74 , 0X74 , 0X44 , 
    0X44 , 0X74 , 0X74 , 0X47 , 
    0X44 , 0X74 , 0X74 , 0X74 , 
    0X44 , 0X74 , 0X74 , 0X77 , 
    0X44 , 0X74 , 0X77 , 0X44 , 
    0X44 , 0X74 , 0X77 , 0X47 , 
    0X44 , 0X74 , 0X77 , 0X74 , 
    0X44 , 0X74 , 0X77 , 0X77 , 
    0X44 , 0X77 , 0X44 , 0X44 , 
    0X44 , 0X77 , 0X44 , 0X47 , 
    0X44 , 0X77 , 0X44 , 0X74 , 
    0X44 , 0X77 , 0X44 , 0X77 , 
    0X44 , 0X77 , 0X47 , 0X44 , 
    0X44 , 0X77 , 0X47 , 0X47 , 
    0X44 , 0X77 , 0X47 , 0X74 , 
    0X44 , 0X77 , 0X47 , 0X77 , 
    0X44 , 0X77 , 0X74 , 0X44 , 
    0X44 , 0X77 , 0X74 , 0X47 , 
    0X44 , 0X77 , 0X74 , 0X74 , 
    0X44 , 0X77 , 0X74 , 0X77 , 
    0X44 , 0X77 , 0X77 , 0X44 , 
    0X44 , 0X77 , 0X77 , 0X47 , 
    0X44 , 0X77 , 0X77 , 0X74 , 
    0X44 , 0X77 , 0X77 , 0X77 , 
    0X47 , 0X44 , 0X44 , 0X44 , 
    0X47 , 0X44 , 0X44 , 0X47 , 
    0X47 , 0X44 , 0X44 , 0X74 , 
    0X47 , 0X44 , 0X44 , 0X77 , 
    0X47 , 0X44 , 0X47 , 0X44 , 
    0X47 , 0X44 , 0X47 , 0X47 , 
    0X47 , 0X44 , 0X47 , 0X74 , 
    0X47 , 0X44 , 0X47 , 0X77 , 
    0X47 , 0X44 , 0X74 , 0X44 , 
    0X47 , 0X44 , 0X74 , 0X47 , 
    0X47 , 0X44 , 0X74 , 0X74 , 
    0X47 , 0X44 , 0X74 , 0X77 , 
    0X47 , 0X44 , 0X77 , 0X44 , 
    0X47 , 0X44 , 0X77 , 0X47 , 
    0X47 , 0X44 , 0X77 , 0X74 , 
    0X47 , 0X44 , 0X77 , 0X77 , 
    0X47 , 0X47 , 0X44 , 0X44 , 
    0X47 , 0X47 , 0X44 , 0X47 , 
    0X47 , 0X47 , 0X44 , 0X74 , 
    0X47 , 0X47 , 0X44 , 0X77 , 
    0X47 , 0X47 , 0X47 , 0X44 , 
    0X47 , 0X47 , 0X47 , 0X47 , 
    0X47 , 0X47 , 0X47 , 0X74 , 
    0X47 , 0X47 , 0X47 , 0X77 , 
    0X47 , 0X47 , 0X74 , 0X44 , 
    0X47 , 0X47 , 0X74 , 0X47 , 
    0X47 , 0X47 , 0X74 , 0X74 , 
    0X47 , 0X47 , 0X74 , 0X77 , 
    0X47 , 0X47 , 0X77 , 0X44 , 
    0X47 , 0X47 , 0X77 , 0X47 , 
    0X47 , 0X47 , 0X77 , 0X74 , 
    0X47 , 0X47 , 0X77 , 0X77 , 
    0X47 , 0X74 , 0X44 , 0X44 , 
    0X47 , 0X74 , 0X44 , 0X47 , 
    0X47 , 0X74 , 0X44 , 0X74 , 
    0X47 , 0X74 , 0X44 , 0X77 , 
    0X47 , 0X74 , 0X47 , 0X44 , 
    0X47 , 0X74 , 0X47 , 0X47 , 
    0X47 , 0X74 , 0X47 , 0X74 , 
    0X47 , 0X74 , 0X47 , 0X77 , 
    0X47 , 0X74 , 0X74 , 0X44 , 
    0X47 , 0X74 , 0X74 , 0X47 , 
    0X47 , 0X74 , 0X74 , 0X74 , 
    0X47 , 0X74 , 0X74 , 0X77 , 
    0X47 , 0X74 , 0X77 , 0X44 , 
    0X47 , 0X74 , 0X77 , 0X47 , 
    0X47 , 0X74 , 0X77 , 0X74 , 
    0X47 , 0X74 , 0X77 , 0X77 , 
    0X47 , 0X77 , 0X44 , 0X44 , 
    0X47 , 0X77 , 0X44 , 0X47 , 
    0X47 , 0X77 , 0X44 , 0X74 , 
    0X47 , 0X77 , 0X44 , 0X77 , 
    0X47 , 0X77 , 0X47 , 0X44 , 
    0X47 , 0X77 , 0X47 , 0X47 , 
    0X47 , 0X77 , 0X47 , 0X74 , 
    0X47 , 0X77 , 0X47 , 0X77 , 
    0X47 , 0X77 , 0X74 , 0X44 , 
    0X47 , 0X77 , 0X74 , 0X47 , 
    0X47 , 0X77 , 0X74 , 0X74 , 
    0X47 , 0X77 , 0X74 , 0X77 , 
    0X47 , 0X77 , 0X77 , 0X44 , 
    0X47 , 0X77 , 0X77 , 0X47 , 
    0X47 , 0X77 , 0X77 , 0X74 , 
    0X47 , 0X77 , 0X77 , 0X77 , 
    0X74 , 0X44 , 0X44 , 0X44 , 
    0X74 , 0X44 , 0X44 , 0X47 , 
    0X74 , 0X44 , 0X44 , 0X74 , 
    0X74 , 0X44 , 0X44 , 0X77 , 
    0X74 , 0X44 , 0X47 , 0X44 , 
    0X74 , 0X44 , 0X47 , 0X47 , 
    0X74 , 0X44 , 0X47 , 0X74 , 
    0X74 , 0X44 , 0X47 , 0X77 , 
    0X74 , 0X44 , 0X74 , 0X44 , 
    0X74 , 0X44 , 0X74 , 0X47 , 
    0X74 , 0X44 , 0X74 , 0X74 , 
    0X74 , 0X44 , 0X74 , 0X77 , 
    0X74 , 0X44 , 0X77 , 0X44 , 
    0X74 , 0X44 , 0X77 , 0X47 , 
    0X74 , 0X44 , 0X77 , 0X74 , 
    0X74 , 0X44 , 0X77 , 0X77 , 
    0X74 , 0X47 , 0X44 , 0X44 , 
    0X74 , 0X47 , 0X44 , 0X47 , 
    0X74 , 0X47 , 0X44 , 0X74 , 
    0X74 , 0X47 , 0X44 , 0X77 , 
    0X74 , 0X47 , 0X47 , 0X44 , 
    0X74 , 0X47 , 0X47 , 0X47 , 
    0X74 , 0X47 , 0X47 , 0X74 , 
    0X74 , 0X47 , 0X47 , 0X77 , 
    0X74 , 0X47 , 0X74 , 0X44 , 
    0X74 , 0X47 , 0X74 , 0X47 , 
    0X74 , 0X47 , 0X74 , 0X74 , 
    0X74 , 0X47 , 0X74 , 0X77 , 
    0X74 , 0X47 , 0X77 , 0X44 , 
    0X74 , 0X47 , 0X77 , 0X47 , 
    0X74 , 0X47 , 0X77 , 0X74 , 
    0X74 , 0X47 , 0X77 , 0X77 , 
    0X74 , 0X74 , 0X44 , 0X44 , 
    0X74 , 0X74 , 0X44 , 0X47 , 
    0X74 , 0X74 , 0X44 , 0X74 , 
    0X74 , 0X74 , 0X44 , 0X77 , 
    0X74 , 0X74 , 0X47 , 0X44 , 
    0X74 , 0X74 , 0X47 , 0X47 , 
    0X74 , 0X74 , 0X47 , 0X74 , 
    0X74 , 0X74 , 0X47 , 0X77 , 
    0X74 , 0X74 , 0X74 , 0X44 , 
    0X74 , 0X74 , 0X74 , 0X47 , 
    0X74 , 0X74 , 0X74 , 0X74 , 
    0X74 , 0X74 , 0X74 , 0X77 , 
    0X74 , 0X74 , 0X77 , 0X44 , 
    0X74 , 0X74 , 0X77 , 0X47 , 
    0X74 , 0X74 , 0X77 , 0X74 , 
    0X74 , 0X74 , 0X77 , 0X77 , 
    0X74 , 0X77 , 0X44 , 0X44 , 
    0X74 , 0X77 , 0X44 , 0X47 , 
    0X74 , 0X77 , 0X44 , 0X74 , 
    0X74 , 0X77 , 0X44 , 0X77 , 
    0X74 , 0X77 , 0X47 , 0X44 , 
    0X74 , 0X77 , 0X47 , 0X47 , 
    0X74 , 0X77 , 0X47 , 0X74 , 
    0X74 , 0X77 , 0X47 , 0X77 , 
    0X74 , 0X77 , 0X74 , 0X44 , 
    0X74 , 0X77 , 0X74 , 0X47 , 
    0X74 , 0X77 , 0X74 , 0X74 , 
    0X74 , 0X77 , 0X74 , 0X77 , 
    0X74 , 0X77 , 0X77 , 0X44 , 
    0X74 , 0X77 , 0X77 , 0X47 , 
    0X74 , 0X77 , 0X77 , 0X74 , 
    0X74 , 0X77 , 0X77 , 0X77 , 
    0X77 , 0X44 , 0X44 , 0X44 , 
    0X77 , 0X44 , 0X44 , 0X47 , 
    0X77 , 0X44 , 0X44 , 0X74 , 
    0X77 , 0X44 , 0X44 , 0X77 , 
    0X77 , 0X44 , 0X47 , 0X44 , 
    0X77 , 0X44 , 0X47 , 0X47 , 
    0X77 , 0X44 , 0X47 , 0X74 , 
    0X77 , 0X44 , 0X47 , 0X77 , 
    0X77 , 0X44 , 0X74 , 0X44 , 
    0X77 , 0X44 , 0X74 , 0X47 , 
    0X77 , 0X44 , 0X74 , 0X74 , 
    0X77 , 0X44 , 0X74 , 0X77 , 
    0X77 , 0X44 , 0X77 , 0X44 , 
    0X77 , 0X44 , 0X77 , 0X47 , 
    0X77 , 0X44 , 0X77 , 0X74 , 
    0X77 , 0X44 , 0X77 , 0X77 , 
    0X77 , 0X47 , 0X44 , 0X44 , 
    0X77 , 0X47 , 0X44 , 0X47 , 
    0X77 , 0X47 , 0X44 , 0X74 , 
    0X77 , 0X47 , 0X44 , 0X77 , 
    0X77 , 0X47 , 0X47 , 0X44 , 
    0X77 , 0X47 , 0X47 , 0X47 , 
    0X77 , 0X47 , 0X47 , 0X74 , 
    0X77 , 0X47 , 0X47 , 0X77 , 
    0X77 , 0X47 , 0X74 , 0X44 , 
    0X77 , 0X47 , 0X74 , 0X47 , 
    0X77 , 0X47 , 0X74 , 0X74 , 
    0X77 , 0X47 , 0X74 , 0X77 , 
    0X77 , 0X47 , 0X77 , 0X44 , 
    0X77 , 0X47 , 0X77 , 0X47 , 
    0X77 , 0X47 , 0X77 , 0X74 , 
    0X77 , 0X47 , 0X77 , 0X77 , 
    0X77 , 0X74 , 0X44 , 0X44 , 
    0X77 , 0X74 , 0X44 , 0X47 , 
    0X77 , 0X74 , 0X44 , 0X74 , 
    0X77 , 0X74 , 0X44 , 0X77 , 
    0X77 , 0X74 , 0X47 , 0X44 , 
    0X77 , 0X74 , 0X47 , 0X47 , 
    0X77 , 0X74 , 0X47 , 0X74 , 
    0X77 , 0X74 , 0X47 , 0X77 , 
    0X77 , 0X74 , 0X74 , 0X44 , 
    0X77 , 0X74 , 0X74 , 0X47 , 
    0X77 , 0X74 , 0X74 , 0X74 , 
    0X77 , 0X74 , 0X74 , 0X77 , 
    0X77 , 0X74 , 0X77 , 0X44 , 
    0X77 , 0X74 , 0X77 , 0X47 , 
    0X77 , 0X74 , 0X77 , 0X74 , 
    0X77 , 0X74 , 0X77 , 0X77 , 
    0X77 , 0X77 , 0X44 , 0X44 , 
    0X77 , 0X77 , 0X44 , 0X47 , 
    0X77 , 0X77 , 0X44 , 0X74 , 
    0X77 , 0X77 , 0X44 , 0X77 , 
    0X77 , 0X77 , 0X47 , 0X44 , 
    0X77 , 0X77 , 0X47 , 0X47 , 
    0X77 , 0X77 , 0X47 , 0X74 , 
    0X77 , 0X77 , 0X47 , 0X77 , 
    0X77 , 0X77 , 0X74 , 0X44 , 
    0X77 , 0X77 , 0X74 , 0X47 , 
    0X77 , 0X77 , 0X74 , 0X74 , 
    0X77 , 0X77 , 0X74 , 0X77 , 
    0X77 , 0X77 , 0X77 , 0X44 , 
    0X77 , 0X77 , 0X77 , 0X47 , 
    0X77 , 0X77 , 0X77 , 0X74 , 
    0X77 , 0X77 , 0X77 , 0X77 , 

};
void encode_byte( uint8_t data, int16_t buffer_index )
{
   int index = data * 4;
   ws_buffer[buffer_index++ ] = leddata[index++];
   ws_buffer[buffer_index++ ] = leddata[index++];
   ws_buffer[buffer_index++ ] = leddata[index++];
   ws_buffer[buffer_index++ ] = leddata[index++];
}
void generate_ws_buffer( uint8_t RData,uint8_t GData,uint8_t BData, int16_t led_no )
{
	//ws2812b
//G--R--B
//MSB first	
   int offset = led_no * 12;
   encode_byte( GData, offset );
   encode_byte( RData, offset+4 );
   encode_byte( BData, offset+8 );   
}
void Send_2812(void)
 {   
#if 1    
    HAL_SPI_Transmit_DMA( &hspi2, ws_buffer, LED_BUFFER_LENGTH ); 
    // wait until finished
    while(__HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_BSY ));
#else
    HAL_SPI_Transmit( &hspi1, ws_buffer, LED_BUFFER_LENGTH, 300 );
#endif
 } 
 
void setAllPixelColorRGB(uint8_t r, uint8_t g, uint8_t b)
{ 
   int i;
   for(i=0;i< LED_NO;i++) {
      generate_ws_buffer( r, g, b, i );
   }
   Send_2812();
}
void setAllPixelColor(uint8_t color[3])
{ 
   int i;
   for(i=0;i< LED_NO;i++) {
      generate_ws_buffer( color[0], color[1], color[2], i );
   }
   Send_2812();
}
 void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b)
 {	 
   generate_ws_buffer( r, g, b, n );
   Send_2812();
}

void k()
{
        int i;
        for(i=0;i<LED_NO;i++)
    {
    if(i>=0&&i<=4) setPixelColor(i,13,10,62);
    if(i==7||i==11||i==13||i==15||i==19) setPixelColor(i,13,10,62);


    HAL_Delay(1);
    }
}

void o()
{
        int i;
        for(i=0;i<10;i++)
    {
    
    uint8_t o[10]={10,19,8,2,6,14,15,23,27,21};
    setPixelColor(o[i],13,12,62);
    
    HAL_Delay(1);
    }
}
void c()
{
        int i;
        for(i=0;i<5;i++)
    {
    
    uint8_t c[5]={10,8,2,6,14};
    setPixelColor(c[i],13,12,62);
    
    HAL_Delay(1);
    }
}


void u()
{
        int i;
        for(i=0;i<10;i++)
    {
    
    uint8_t u[10]={0,1,2,6,14,15,23,27,28,29};
    setPixelColor(u[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void r()
{
        int i;
        for(i=0;i<10;i++)
    {
    
    uint8_t r[10]={0,1,2,3,4,9,11,7,13,15};
    setPixelColor(r[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void j()
{
        int i;
        for(i=0;i<9;i++)
    {
    
    uint8_t j[9]={20,21,22,23,24,19,15,14,13};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void a()
{
        int i;
        for(i=0;i<12;i++)
    {
    
    uint8_t j[12]={4,3,2,8,10,18,22,23,24,7,12,17};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void z()
{
        int i;
        for(i=0;i<13;i++)
    {
    
    uint8_t j[13]={0,9,10,19,20,18,12,6,4,5,14,15,24};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void p()
{
        int i;
        for(i=0;i<13;i++)
    {
    
    uint8_t j[13]={0,1,2,3,4,9,10,7,12,18};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void e()
{
        int i;
        for(i=0;i<14;i++)
    {
    
    uint8_t j[14]={0,1,2,3,4,9,19,19,7,12,5,14,15,10};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void d()
{
        int i;
        for(i=0;i<10;i++)
    {
    
    uint8_t j[10]={0,1,2,3,4,5,13,12,11,9};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}

void l()
{
        int i;
        for(i=0;i<8;i++)
    {
    
    uint8_t j[8]={0,1,2,3,4,5,14,15};
    setPixelColor(j[i],13,12,62);
    
    HAL_Delay(1);
    }
}
void print_jarczak(uint16_t speed)
{
        HAL_Delay(500);
    setAllPixelColorRGB(0,0,0);
    j();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    a();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    r();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    c();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    z();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    a();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    k();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    p();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    e();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    d();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    a();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
    l();
    HAL_Delay(speed);
    setAllPixelColorRGB(0,0,0);
}




void initLEDMOSI(void)
{
   uint8_t buffer0[2] = { 0, 0 };
   HAL_SPI_Transmit(&hspi2, buffer0, 1, 100 );
}
void fadeRGB(uint16_t speed)
{
  int i;
  uint8_t r=0,g=0,b=0;
 
      for(i=0;i<250;i++)
      {
      setAllPixelColorRGB(r,g,b+i);
      HAL_Delay(speed);
      }
       for(i=0;i<250;i++)
      {
      setAllPixelColorRGB(r,g,b-i);
      HAL_Delay(speed);
      }
       for(i=0;i<250;i++)
      {
      setAllPixelColorRGB(r+i,g,b);
      HAL_Delay(speed);
      }
       for(i=0;i<250;i++)
      {
      setAllPixelColorRGB(r-i,g,b);
      HAL_Delay(speed);
      }
       for(i=0;i<250;i++)
      {
      setAllPixelColorRGB(r,g+i,b);
      HAL_Delay(speed);
      }
       for(i=0;i<250;i++)
      {
      setAllPixelColorRGB(r,g-i,b);
      HAL_Delay(speed);
      }

    
}
void fade(uint16_t speed, uint8_t r, uint8_t g, uint8_t b)
{
  int i,j,k;
   for(i=0,j=0,k=0;i<r||j<g||k<b;i++,j++,k++)
    {
      setAllPixelColorRGB(r-i,g-j,b-k);
      HAL_Delay(speed);
    }
    for(i=0,j=0,k=0;i<r||j<g||k<b;i++,j++,k++)
    {
      setAllPixelColorRGB(r+i,g+j,b+k);
      HAL_Delay(speed);
    }
  
   


}

void reset()
{
    setAllPixelColorRGB(0,0,0);
}





























