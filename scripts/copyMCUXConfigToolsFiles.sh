#! /bin/sh

mv ../board/pin_mux.c ../src/system/pin_mux.c
mv ../board/pin_mux.h ../src/system/pin_mux.h

mv ../board/clock_config.c ../src/system/clock_config.c
mv ../board/clock_config.h ../src/system/clock_config.h

mv ../board/peripherals.c ../src/system/peripherals.c
mv ../board/peripherals.h ../src/system/peripherals.h

rm -rf ../board/
