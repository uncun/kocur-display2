

 #ifndef lcd_H
 #define lcd_H
#include "stm32f4xx_hal.h"
#include "stdlib.h"
#define LCD_ROWS 4
#define LCD_COLS 20

#define LCD_LINE1 0x00
#define LCD_LINE2 0x40
#define LCD_LINE3 0x14
#define LCD_LINE4 0x54

#define LCDC_SET_CGRAM 0x40
#define LCDC_SET_DDRAM 0x80

void lcd_init (void);   // initialize lcd

void lcd_send_cmd (char cmd);  // send command to the lcd

void lcd_send_data (char data);  // send data to the lcd

void lcd_send_string (char *str);  // send string to the lcd

void lcd_clear (void);  

void lcd_locate (uint8_t x, uint8_t y);

void lcd_str_XY(uint8_t x, uint8_t y, char * str);

void set_led(void);

void reset_led(void);


void buf_locate(uint8_t x, uint8_t y);
void buf_char(char c);
void buf_char(char c);
void buf_clear_menu(void);
void lcd_refresh(void);
void buf_str(char *text);
void buf_str_XY(uint8_t x, uint8_t y, char *text);
void buf_int(uint8_t value);
#endif