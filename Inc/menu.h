
 #ifndef menu_H
 #define menu_H
typedef struct menu_struct menu_t;

struct menu_struct 
{
    const char * name;
    menu_t * next;
    menu_t * prev;
    menu_t * child;
    menu_t * parent;
    void (*menu_fun)(void); 
};

menu_t menu1; 
    menu_t sub_menu1_1;
    menu_t sub_menu1_2;
menu_t menu2;
    menu_t sub_menu2_1;
    menu_t sub_menu2_2;

menu_t menu3;
menu_t menu4;
menu_t menu5;
menu_t menu6;

void menu_refresh(void);
void menu_next(void);
void menu_prev(void);
void menu_enter(void);
void menu_back(void);
void duty_callback(void);


#endif