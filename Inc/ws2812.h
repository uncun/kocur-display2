 #ifndef ws2812_H
 #define ws2812_H
//  #include "ws2812.c"
#include "stm32f4xx_hal.h"
#define LED_NO    30
#define LED_BUFFER_LENGTH (LED_NO*12)
void fade(uint16_t speed, uint8_t r, uint8_t g, uint8_t b);
void fadeRGB(uint16_t speed);
void initLEDMOSI(void);
void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
void setAllPixelColor(uint8_t color[3]);
void setAllPixelColorRGB(uint8_t r, uint8_t g, uint8_t b);
void Send_2812(void);
void generate_ws_buffer( uint8_t RData,uint8_t GData,uint8_t BData, int16_t led_no );
void encode_byte( uint8_t data, int16_t buffer_index );
void k();
void o();
void c();
void u();
void r();
void j();
void a();
void z();
void p();
void e();
void d();
void l();
void reset();

 #endif

