# How to build

1. First go to *build* folder
2. Type ``cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/cortex-m0plus
   -DCMAKE_BUILD_TYPE=Debug ..`` to make cmake generate build system based on
   makefiles. You can also change ``Debug`` to ``Release``, etc. (TODO complete
   this list)
3. Type ``make`` without leaving the *build* folder to compile program and more
   (coming soon!)
